<?php

namespace go\goBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($page)
    {
        return $this->render('gogoBundle:Default:index.html.twig', array('name' => $page));
    }
}
