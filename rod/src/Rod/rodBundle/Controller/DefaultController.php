<?php

namespace Rod\rodBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction($name)
    {

        return $this->render('RodrodBundle:Default:web.html.twig', array('name' => $name));
    }
    public function webAction($rod)
    {

        return new Response ('el numero es = ' . $rod);
    }
    
}
